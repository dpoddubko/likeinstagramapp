package com.dpoddubko.likeinstagramapp

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.dpoddubko.likeinstagramapp.bases.BaseFragment
import com.dpoddubko.likeinstagramapp.bases.HasNavigationManager
import com.dpoddubko.likeinstagramapp.features.add.AddFragment
import com.dpoddubko.likeinstagramapp.features.add.FavoritesFragment
import com.dpoddubko.likeinstagramapp.features.home.HomeFragment
import com.dpoddubko.likeinstagramapp.features.profile.ProfileFragment
import com.dpoddubko.likeinstagramapp.features.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity(), HasNavigationManager
    , HomeFragment.OnHomeFragmentInteractionListener
    , AddFragment.OnAddFragmentInteractionListener
    , SearchFragment.OnSearchFragmentInteractionListener
    , FavoritesFragment.OnFavoritesFragmentInteractionListener
    , ProfileFragment.OnProfileFragmentInteractionListener {

    lateinit var mNavigationManager: NavigationManager
    var mCurrentFragment: BaseFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        mNavigationManager = NavigationManager(supportFragmentManager, R.id.container)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (savedInstanceState == null) {
            mNavigationManager.openAsRoot(HomeFragment.newInstance())
            navigation.menu.getItem(0).isChecked
        }

        sendToolbarButton.setOnClickListener {
            toast("sendToolbarButton clicked")

        }
        cameraToolbarButton.setOnClickListener {
            toast("cameraToolbarButton clicked")
        }
    }

    override fun provideNavigationManager(): NavigationManager = mNavigationManager

    override fun setCurrentFragment(fragment: BaseFragment) {
        mCurrentFragment = fragment
    }

    override fun setBottomNavigation(show: Boolean, menuId: Int) {
        if (show) {
            navigation.visibility = View.VISIBLE
            when (menuId) {
                R.id.navigation_home -> navigation.menu.getItem(0).isChecked = true
                R.id.navigation_search -> navigation.menu.getItem(1).isChecked = true
                R.id.navigation_add -> navigation.menu.getItem(2).isChecked = true
                R.id.navigation_favorite -> navigation.menu.getItem(3).isChecked = true
                R.id.navigation_profile -> navigation.menu.getItem(4).isChecked = true
            }

        } else
            navigation.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (mCurrentFragment == null || !mCurrentFragment!!.onBackPressed())
            super.onBackPressed()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                mNavigationManager.open(HomeFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                mNavigationManager.open(SearchFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_add -> {
                mNavigationManager.open(AddFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorite -> {
                mNavigationManager.open(FavoritesFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                mNavigationManager.open(ProfileFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

}

package com.dpoddubko.likeinstagramapp.bases

interface FragmentInteractionListener {

    fun setCurrentFragment(fragment: BaseFragment)

    fun setBottomNavigation(show: Boolean,menuId:Int)
}
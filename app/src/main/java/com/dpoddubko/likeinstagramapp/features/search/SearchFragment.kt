package com.dpoddubko.likeinstagramapp.features.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.bases.BaseFragment
import com.dpoddubko.likeinstagramapp.bases.FragmentInteractionListener

class SearchFragment : BaseFragment() {

    lateinit var mListener: OnSearchFragmentInteractionListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnSearchFragmentInteractionListener) mListener = context else {
            throw RuntimeException(context!!.toString() + " must implement OnSearchFragmentInteractionListener")
        }
    }

    override fun onStart() {
        super.onStart()
        mListener.setBottomNavigation(true, R.id.navigation_search)
    }

    interface OnSearchFragmentInteractionListener : FragmentInteractionListener

    companion object {
        fun newInstance(): SearchFragment {
           return SearchFragment()
        }
    }
}

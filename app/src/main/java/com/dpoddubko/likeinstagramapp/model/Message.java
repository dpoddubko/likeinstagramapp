package com.dpoddubko.likeinstagramapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Message {

    @SerializedName("liked_by")
    private String likedBy;
    private int id;
    private String title;
    private List<String> photos = new ArrayList<>();
    private String tags;
    private String lasted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(String likedBy) {
        this.likedBy = likedBy;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getLasted() {
        return lasted;
    }

    public void setLasted(String lasted) {
        this.lasted = lasted;
    }

}

package com.dpoddubko.likeinstagramapp.features.home

interface PostsAdapterDelegate {
    fun onLikeButtonClicked(position: Int)
    fun onMessageButtonClicked(position: Int)
    fun onSendButtonClicked(position: Int)
    fun onStoreButtonClicked(position: Int)
    fun onMoreButtonClicked(position: Int)
}

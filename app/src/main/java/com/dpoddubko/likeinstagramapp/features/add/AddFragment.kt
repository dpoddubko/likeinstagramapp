package com.dpoddubko.likeinstagramapp.features.add

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.bases.BaseFragment
import com.dpoddubko.likeinstagramapp.bases.FragmentInteractionListener

class AddFragment : BaseFragment() {

    lateinit var mListener: OnAddFragmentInteractionListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnAddFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAddFragmentInteractionListener")
        }
    }

    override fun onStart() {
        super.onStart()

        mListener.setBottomNavigation(true, R.id.navigation_add)

    }

    interface OnAddFragmentInteractionListener : FragmentInteractionListener

    companion object {
        fun newInstance(): AddFragment {
            val fragment = AddFragment()
            return fragment
        }
    }
}

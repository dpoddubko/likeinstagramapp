package com.dpoddubko.likeinstagramapp.bases

import com.dpoddubko.likeinstagramapp.NavigationManager

interface HasNavigationManager {
    fun provideNavigationManager(): NavigationManager?
}
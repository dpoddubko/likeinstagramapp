package com.dpoddubko.likeinstagramapp.features.add

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.bases.BaseFragment
import com.dpoddubko.likeinstagramapp.bases.FragmentInteractionListener

class FavoritesFragment : BaseFragment() {

    lateinit var mListener: OnFavoritesFragmentInteractionListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorits, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFavoritesFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement OnAddFragmentInteractionListener")
        }
    }
    interface OnFavoritesFragmentInteractionListener : FragmentInteractionListener

    override fun onStart() {
        super.onStart()
        mListener.setBottomNavigation(true, R.id.navigation_favorite)
    }

    companion object {
        fun newInstance(): FavoritesFragment {
            return FavoritesFragment()
        }
    }
}

package com.dpoddubko.likeinstagramapp.features.home.adapter

import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.features.home.PostsAdapterDelegate

class PostsViewHolder(itemView: View, private val delegate: PostsAdapterDelegate) :
    RecyclerView.ViewHolder(itemView) {

    val profileImage: ImageView = itemView.findViewById(R.id.profileImage)
    val userName: TextView = itemView.findViewById(R.id.userName)
    val location: TextView = itemView.findViewById(R.id.location)
    val likedBy: TextView = itemView.findViewById(R.id.likedBy)
    val tags: TextView = itemView.findViewById(R.id.tags)
    val lasted: TextView = itemView.findViewById(R.id.lasted)

    private val likeButton: ImageButton = itemView.findViewById(R.id.likeButton)
    private  val messageButton: ImageButton = itemView.findViewById(R.id.messageButton)
    private val sendButton: ImageButton = itemView.findViewById(R.id.sendButton)
    private val storeButton: ImageButton = itemView.findViewById(R.id.storeButton)
    private val moreButton: ImageButton = itemView.findViewById(R.id.moreButton)

    val pager: ViewPager = itemView.findViewById(R.id.imagesPager)
    val tabLayout: TabLayout = itemView.findViewById(R.id.dots)

    init {
        likeButton.setOnClickListener(this::onLikeButtonClick)
        messageButton.setOnClickListener(this::onMessageButtonClick)
        sendButton.setOnClickListener(this::onSendButtonClick)
        storeButton.setOnClickListener(this::onStoreButtonClick)
        moreButton.setOnClickListener(this::onMoreButtonClick)
    }

    private fun onMessageButtonClick(v: View) {
        val position = adapterPosition
        delegate.onMessageButtonClicked(position)
    }

    private fun onSendButtonClick(v: View) {
        val position = adapterPosition
        delegate.onSendButtonClicked(position)
    }

    private fun onStoreButtonClick(v: View) {
        val position = adapterPosition
        delegate.onStoreButtonClicked(position)
    }

    private fun onLikeButtonClick(v: View) {
        val position = adapterPosition
        delegate.onLikeButtonClicked(position)
    }

    private fun onMoreButtonClick(v: View) {
        val position = adapterPosition
        delegate.onMoreButtonClicked(position)
    }


}
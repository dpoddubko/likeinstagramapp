package com.dpoddubko.likeinstagramapp.features.profile

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dpoddubko.likeinstagramapp.NavigationManager
import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.bases.BaseFragment
import com.dpoddubko.likeinstagramapp.bases.FragmentInteractionListener
import com.dpoddubko.likeinstagramapp.bases.HasNavigationManager

class ProfileFragment : BaseFragment(), HasNavigationManager {

    lateinit var mListener: OnProfileFragmentInteractionListener

    lateinit var mNavigationManager: NavigationManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnProfileFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement OnProfileFragmentInteractionListener")
        }

        mNavigationManager = NavigationManager(childFragmentManager, R.id.container)
    }

    interface OnProfileFragmentInteractionListener : FragmentInteractionListener

    override fun onStart() {
        super.onStart()

        mListener.setBottomNavigation(true, R.id.navigation_profile)
    }

    override fun provideNavigationManager(): NavigationManager = mNavigationManager

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }
}
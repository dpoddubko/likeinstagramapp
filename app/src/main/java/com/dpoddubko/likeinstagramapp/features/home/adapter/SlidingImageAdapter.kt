package com.dpoddubko.likeinstagramapp.features.home.adapter

import android.content.Context
import android.os.Parcelable
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.dpoddubko.likeinstagramapp.R
import com.squareup.picasso.Picasso

class SlidingImageAdapter(context: Context?, imageModelArrayList: List<String>) : PagerAdapter() {
    private val imageModelArrayList: List<String>
    private val inflater: LayoutInflater
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false)!!
        val imageView = imageLayout
            .findViewById<View>(R.id.image) as ImageView
        Picasso
            .get()
            .load(imageModelArrayList[position])
            .centerCrop()
            .fit()
            .into(imageView)
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}
    override fun saveState(): Parcelable? {
        return null
    }

    init {
        this.imageModelArrayList = imageModelArrayList
        inflater = LayoutInflater.from(context)
    }
}
package com.dpoddubko.likeinstagramapp.data

import android.app.Application
import com.dpoddubko.likeinstagramapp.model.Posts
import com.google.gson.Gson

fun getPosts(application: Application?): Posts? {
    val fileName = "posts.json"
    val jsonString = application?.assets?.open(fileName)?.bufferedReader().use {
        it?.readText()
    }
    return Gson().fromJson(jsonString, Posts::class.java)
}
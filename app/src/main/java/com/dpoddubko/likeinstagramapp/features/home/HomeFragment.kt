package com.dpoddubko.likeinstagramapp.features.home

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.bases.BaseFragment
import com.dpoddubko.likeinstagramapp.bases.FragmentInteractionListener
import com.dpoddubko.likeinstagramapp.data.getPosts
import com.dpoddubko.likeinstagramapp.features.home.adapter.PostsAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.support.v4.toast

class HomeFragment : BaseFragment(), PostsAdapterDelegate {

    override fun onMoreButtonClicked(position: Int) {
        toast("onMoreButtonClicked position = $position")
    }

    override fun onMessageButtonClicked(position: Int) {
        toast("onMessageButtonClicked position = $position")
    }

    override fun onSendButtonClicked(position: Int) {
        toast("onSendButtonClicked position = $position")
    }

    override fun onStoreButtonClicked(position: Int) {
        toast("onStoreButtonClicked position = $position")
    }

    override fun onLikeButtonClicked(position: Int) {
        toast("onLikeButtonClicked position = $position")
    }

    lateinit var mListener: OnHomeFragmentInteractionListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val posts = getPosts(activity?.application)

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        val postsAdapter = posts?.posts?.let { PostsAdapter(it, this) }
        recyclerView.adapter = postsAdapter
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnHomeFragmentInteractionListener) mListener = context else {
            throw RuntimeException(context?.toString() + " must implement OnHomeFragmentInteractionListener")
        }
    }

    interface OnHomeFragmentInteractionListener : FragmentInteractionListener

    override fun onStart() {
        super.onStart()
        mListener.setBottomNavigation(true, R.id.navigation_home)
    }

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }
}

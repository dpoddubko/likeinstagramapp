package com.dpoddubko.likeinstagramapp.features.home.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dpoddubko.likeinstagramapp.R
import com.dpoddubko.likeinstagramapp.features.home.PostsAdapterDelegate
import com.dpoddubko.likeinstagramapp.model.Post
import com.squareup.picasso.Picasso


class PostsAdapter(
    private val posts: List<Post>,
    private val delegate: PostsAdapterDelegate
) :
    RecyclerView.Adapter<PostsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.post_item, parent, false)
        return PostsViewHolder(itemView, delegate)
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {

        val item = posts[position]
        holder.userName.text = item.userInfo.name
        holder.location.text = item.userInfo.location

        holder.likedBy.text = item.message.likedBy
        holder.tags.text = item.message.tags
        holder.lasted.text = item.message.lasted
        holder.pager.adapter = SlidingImageAdapter(holder.userName.context, item.message.photos)

        holder.tabLayout.setupWithViewPager(holder.pager)
        Picasso.get().load(item.userInfo.avatarUrl).into(holder.profileImage)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return posts.size
    }
}
